/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programa3;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author a968692
 */
public class Programa3 {

    /**
     * @param args the command line arguments
     */
    private static int somaX(ArrayList<Dados> dados) {
        int soma = 0;
        for (Dados dado : dados) {
            soma += dado.getX();
        }
        return soma;
    }

    private static int somaY(ArrayList<Dados> dados) {
        int soma = 0;
        for (Dados dado : dados) {
            soma += dado.getY();
        }
        return soma;
    }

    private static int quadradoY(ArrayList<Dados> dados) {
        int somaQuadrado = 0;
        for (Dados dado : dados) {
            somaQuadrado += Math.pow(dado.getY(), 2);
        }
        return somaQuadrado;
    }

    private static int xTimesY(ArrayList<Dados> dados) {
        int mult = 0;
        for (Dados dado : dados) {
            mult += dado.getX() * dado.getY();
        }
        return mult;
    }

    private static int quadradoX(ArrayList<Dados> dados) {
        int somaQuadrado = 0;
        for (Dados dado : dados) {
            somaQuadrado += Math.pow(dado.getX(), 2);
        }
        return somaQuadrado;
    }

    public static float xAverage(float somaX, int size) {
        return somaX / size;
    }

    public static float yAverage(float somaY, int size) {
        return somaY / size;
    }

    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<Dados> dados = new ArrayList<Dados>();
        int numEntradas = Integer.parseInt(JOptionPane.showInputDialog("Numero de entradas: "));
        int loc = Integer.parseInt(JOptionPane.showInputDialog("Numero de Linhas: "));
        int cont = 0;
        while (cont < numEntradas) {
            Dados d = new Dados();
            d.setX(Integer.parseInt(JOptionPane.showInputDialog("Digite X: ")));
            d.setY(Integer.parseInt(JOptionPane.showInputDialog("Digite y ")));
            dados.add(d);
            cont++;
        }
        float somaX = somaX(dados);
        float somaY = somaY(dados);
        float xquadrado = quadradoX(dados);
        float yquadrado = quadradoY(dados);
        float xTimesY = xTimesY(dados);
        float xAverage = xAverage(somaX, dados.size());
        float yAverage = yAverage(somaY, dados.size());
        float beta1 = (xTimesY - (numEntradas * xAverage * yAverage)) / (xquadrado - (numEntradas * (xAverage * xAverage)));
        float beta0 = (yAverage - (beta1 * xAverage));
        float aux = (float) ((numEntradas * xquadrado) - (somaX * somaX)) * ((numEntradas * yquadrado) - (somaY * somaY));
        float rXY = ((numEntradas * xTimesY) - (somaX * somaY)) / aux;
        float rQuadrado = (float) Math.pow(rXY, 2);
        float yK = beta0 + (beta1 * loc);
        System.out.println("Somatoria x: " + somaX);
        System.out.println("Somatoria y: " + somaY);
        System.out.println("Somatoria x quadrado: " + xquadrado);
        System.out.println("Somatoria y quadrado: " + yquadrado);
        System.out.println("X times Y: " + xTimesY);
        System.out.println("X avg: " + xAverage);
        System.out.println("Y avg: " + yAverage);
        System.out.println("Beta1: " + beta1);
        System.out.println("Beta2: " + beta0);
        System.out.println("Rxy: " + rXY);
        System.out.println("R²: " + rQuadrado);
        System.out.println("Yk: " + yK);
    }

}
